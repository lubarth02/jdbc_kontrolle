import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ZugangsdatenRepoImpl implements ZugangsdatenRepository {

    private Connection conn;

    public ZugangsdatenRepoImpl() throws SQLException, ClassNotFoundException {
        this.conn = MySqLDBVerbindung.getConnection("jdbc:mysql://localhost:3306/zugangsdaten", "root", "");
    }

    @Override
    public Optional<Zugangsdaten> insert(Zugangsdaten entity) {
        try {
            EigeneAsserts.notNull(entity);

            String sql = "INSERT INTO `zugangsdaten` (`username`, `password`, `url`) VALUES (?, ?, ?)";
            PreparedStatement statement = conn.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            statement.setString(1, entity.getUsername());
            statement.setString(2, entity.getPasswort());
            statement.setString(3, entity.getUrl());

            int rowsAffected = statement.executeUpdate();
            if (rowsAffected == 0) {
                return Optional.empty();
            }

            ResultSet generatedkeys = statement.getGeneratedKeys();
            if (generatedkeys.next()) {
                return this.getById(generatedkeys.getLong(1));
            } else {
                return Optional.empty();
            }

        } catch (SQLException sqlException) {
            throw new DatenbankException(sqlException.getMessage());
        }
    }

    private int countZugangsdatenInDbWithId(Long id) {
        try {
            String countSql = "SELECT COUNT(*) FROM `zugangsdaten` WHERE `id`=?";
            PreparedStatement statement = conn.prepareStatement(countSql);
            statement.setLong(1, id);
            ResultSet resultSetCount = statement.executeQuery();
            resultSetCount.next();
            int zugangsdatenCount = resultSetCount.getInt(1);
            return zugangsdatenCount;

        } catch (SQLException sqlException) {
            throw new DatenbankException(sqlException.getMessage());
        }

    }

    @Override
    public Optional<Zugangsdaten> getById(Long id) {
        EigeneAsserts.notNull(id);

        if (countZugangsdatenInDbWithId(id) == 0) {
            return Optional.empty();
        } else {
            String sql = "SELECT * FROM `zugangsdaten` WHERE id=?";

            try {
                PreparedStatement statement = conn.prepareStatement(sql);
                statement.setLong(1, id);
                ResultSet resultSet = statement.executeQuery();
                List<Zugangsdaten> studentList = buildZugangsdaten(resultSet);
                return Optional.of(studentList.get(0)); //Zugangsdaten an Position 0 wird zurückgegeben da nur 1 Zugangsdaten pro id vorhanden ist.

            } catch (SQLException sqlException) {
                throw new DatenbankException(sqlException.getMessage());
            }
        }
    }

    private List<Zugangsdaten> buildZugangsdaten(ResultSet resultSet) {
        try {
            List<Zugangsdaten> zugangsdatenList = new ArrayList<>();

            while (resultSet.next()) {
                zugangsdatenList.add(new Zugangsdaten(
                        resultSet.getLong("id"),
                        resultSet.getString("username"),
                        resultSet.getString("password"),
                        resultSet.getString("url")
                ));
            }
            return zugangsdatenList;
        } catch (SQLException sqlException) {
            throw new DatenbankException(sqlException.getMessage());
        }
    }

    @Override
    public List<Zugangsdaten> getAll() {
        try {
            String sql = "SELECT * FROM `zugangsdaten`";
            List<Zugangsdaten> zugangsdatenList = new ArrayList<>();
            PreparedStatement statement = conn.prepareStatement(sql);
            ResultSet resultSet = statement.executeQuery();
            return buildZugangsdaten(resultSet);

        } catch (SQLException sqlException) {
            throw new DatenbankException(sqlException.getMessage());
        }

    }

    @Override
    public List<Zugangsdaten> getAllWithUrlContaining(String urlpart) {
        try {
            List<Zugangsdaten> zugangsdatenList = new ArrayList<>();
            String sql = "SELECT * FROM `zugangsdaten` WHERE LOWER(`url`) LIKE ?";
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1,"%"+urlpart+"%");
            ResultSet resultSet = statement.executeQuery();
            zugangsdatenList = buildZugangsdaten(resultSet);
            return zugangsdatenList;
        }
        catch (SQLException sqlException){
            throw new DatenbankException(sqlException.getMessage());
        }
    }
}
