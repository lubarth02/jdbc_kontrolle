
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

public class Cli {

    Scanner scanner;
    ZugangsdatenRepository repoZugangsdaten;


    public Cli(ZugangsdatenRepoImpl repoZugangsdaten) {
        this.scanner = new Scanner(System.in);
        this.repoZugangsdaten = repoZugangsdaten;
    }

    public void start() {
        String input = "-";
        while (!input.equals("x")) {
            showMeneu();
            input = scanner.nextLine();
            switch (input) {
                case "1":
                    addZugangsdaten();
                    break;
                case "2":
                    showAllZugangsdaten();
                    break;
                case "3":
                    showZugangsdatenWithId();
                    break;
                case "4":
                    showZugangsdatenWithUrl();
                case "x":
                    System.out.println("Auf Wiedersehen");
                    break;

                default:
                    inputError();
                    break;
            }
        }
        scanner.close();

    }

    private void showZugangsdatenWithUrl() throws FalscherWertException {
        System.out.println("****** nach Url suchen ******");
        System.out.println("Bitte Suchberiff eingeben");
        String url = scanner.nextLine();
        if (url.equals("")) throw new FalscherWertException("URL darf nicht leer sein");
        List<Zugangsdaten> zugangsdatenList = repoZugangsdaten.getAllWithUrlContaining(url);
        for (Zugangsdaten zugangsdaten : zugangsdatenList){
            System.out.println(zugangsdaten);
        }
    }

    private void showZugangsdatenWithId() {
        System.out.println("****** Zugangsdaten per id anzeigen ******");
        Optional<Zugangsdaten> zugangsdaten = repoZugangsdaten.getById(Long.valueOf(1));
        System.out.println(zugangsdaten.get());
    }

    private void showAllZugangsdaten() {
        System.out.println("****** alle Daten anzeigen ******");
        List<Zugangsdaten> zugangsdatenList = repoZugangsdaten.getAll();
        for (Zugangsdaten zugangsdaten : zugangsdatenList){
            System.out.println(zugangsdaten);
        }
    }

    private void addZugangsdaten() {
        System.out.println("****** Zugangsdaten eifügen ******");
        Scanner scanner = new Scanner(System.in);

        System.out.println("Username eingeben");
        String username = scanner.nextLine();
        if (username.equals("")) throw new FalscherWertException("Username darf nicht leer sein");

        System.out.println("Passwort eingeben");
        String password = scanner.nextLine();
        if (password.equals("")) throw new FalscherWertException("Passwort darf nicht leer sein");

        System.out.println("URL eingeben");
        String url = scanner.nextLine();
        if (url.equals("")) throw new FalscherWertException("URL darf nicht leer sein");

        repoZugangsdaten.insert(new Zugangsdaten(null,username,password,url));
    }

    private void showMeneu() {
        System.out.println("---------------Kursmanagement-----------");
        System.out.println("(1) Zugangsdaten eingeben \t (2) Alle Zugangsdaten anzeigen\t (3) Zugangsdaten per ID anzeigen \t");
        System.out.println("(4) Nach URL suchen \t ");
        System.out.println("(x) Ende");
    }
    private void inputError() {
        System.out.println("Bitte nur die Zahlen der Menüauswahl eingeben!");
    }
}