import java.sql.SQLException;

public class Main {
    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        Cli cli = new Cli(new ZugangsdatenRepoImpl());
        cli.start();
    }
}
